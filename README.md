# unitunes

projeto ainda não finalizado.

código-fonte disponível em [bitbucket.org/ssjr/unitunes](https://bitbucket.org/ssjr/unitunes)
exemplo rodando online: [unitunes-sergio.herokuapp.com](https://unitunes-sergio.herokuapp.com)

logins pré-cadastrados com senha "123123123":
admin@unitunes.br
author@unitunes.br
academic@unitunes.br

# setup

Testado com Ruby 2.4+
PostgreSQL 9.6

```
gem install bundler
bundle install
# configurar o arquivo config/database.yml com credenciais do postgres
bin/rails s # inicia o servidor em http://localhost:3000
```
