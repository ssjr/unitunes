class CreditsController < ApplicationController
  def index
    authorize(Credit)
    @credits = current_user.credits
  end

  def new
    authorize(:credit)
    @credit = PaymentForm.new()
  end

  def create
    payment = SomeMagicClassOfPayment.new(params: credit_params, user: current_user)
    authorize(:credit)
    if payment.create
      redirect_to credits_path, notice: 'Success'
    else
      render :new, notice: 'Failure'
    end
  end

  private

  def credit_params
    params.require(:credit).permit(:amount)
  end
end
