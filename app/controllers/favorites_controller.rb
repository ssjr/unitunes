class FavoritesController < MediasController
  def index
    authorize(Media)
    @favorites = current_user.favorites.all
  end
end
