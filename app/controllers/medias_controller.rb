class MediasController < ApplicationController
  before_action :detect_media_type
  before_action :set_media, only: [:show, :edit, :update, :destroy, :favorite, :purchase]

  def index
    authorize(Media)
    @media = @type.all
    render "#{@kind}/index"
  end

  # GET /media/1
  # GET /media/1.json
  def show
    authorize(@media.becomes(Media))
  end


  def new
    @media = @type.new
    authorize(@media.becomes(Media))
  end

  def edit
    authorize(@media.becomes(Media))
  end

  def create
    @media = @type.new(media_params)
    @media.author = current_user
    authorize(@media.becomes(Media))

    respond_to do |format|
      if @media.save
        format.html { redirect_to @media, notice: 'Media was successfully created.' }
        format.json { render :show, status: :created, location: @media }
      else
        format.html { render :new }
        format.json { render json: @media.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /media/1
  # PATCH/PUT /media/1.json
  def update
    authorize(@media.becomes(Media))
    respond_to do |format|
      if @media.update(media_params)
        format.html { redirect_to @media, notice: 'Media was successfully updated.' }
        format.json { render :show, status: :ok, location: @media }
      else
        format.html { render :edit }
        format.json { render json: @media.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /media/1
  # DELETE /media/1.json
  def destroy
    authorize(@media.becomes(Media))
    @media.disable("Removido")
    respond_to do |format|
      format.html { redirect_to media_index_url, notice: 'Media was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def favorite
    authorize(@media)
    existing = current_user.favorites.where(media: @media).destroy_all
    if existing.empty?
      current_user.favorites.create(media: @media)
    end
    respond_to do |format|
      format.html { redirect_back(fallback_location: root_path) }
      format.json { render nothing: true, status: :created }
    end
  end

  def purchase
    authorize(@media)
    pr = current_user.purchases.create(media: @media, price: @media.price)
    if
      redirect_to @media, notice: 'Success'
    else
      redirect_back fallback_location: @media, notice: 'Failure'
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_media
    @media = @type.find(params[:id] || params["#{@param_key}_id"])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def media_params
    params.require(@param_key).permit(:name, :description, :image, :price, :author, :category, :size)
  end

  def detect_media_type
    @kind = request.path.match(/^\/([^\/]+)/).captures.first
    @param_key = @kind.singularize
    @type = @param_key.classify.constantize
  end
end
