class PagesController < ApplicationController
  def dashboard
    @purchases = Credit.where('"credits"."amount" > 0').all
    authorize(:dashboard)
  end
end
