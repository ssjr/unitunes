class PaymentForm
  include ActiveModel::Model

  attr_accessor :amount, :type, :name, :number, :cvv, :date

  def initialize(params = {})
    %i(amount type name number cvv date).each do |key|
      send("#{key}=", params[key])
    end
  end

  def valid?
    true
  end
end
