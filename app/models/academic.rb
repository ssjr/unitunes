class Academic < User
  after_touch :check_if_have_published_media

  private

  def check_if_have_published_media
    update_column(:type, Author) if medias.exists?
  end
end
