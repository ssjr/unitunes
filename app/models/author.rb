class Author < User
  after_touch :check_if_have_published_media

  private

  def check_if_have_published_media
    update_column(:type, Academic) if medias.count.zero?
  end
end
