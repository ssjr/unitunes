class Book < Media
  CATEGORIES = [
    "Arts & Entertainment",
    "Bopgraphies & Memoirs",
    "Books in Spanish",
    "Business & Personal Finance",
    "Children & Teens",
    "Comics & Graphic Novels",
    "Computers & Internet",
    "Cookbooks, Food & Wine",
    "Education",
    "Fiction & Literature",
    "Health, Mind & Body",
    "History",
    "Humor",
    "Lifestyle & Home",
    "Mysteries & Thrillers",
    "Nonfiction",
    "Parenting",
    "Politics & Current Events",
    "Professional & Technical",
    "Reference",
    "Religion & Spirituality",
    "Romance",
    "Sci-Fy & Fantasy",
    "Science & Nature",
    "Sports & Outdoors",
    "Textbooks",
    "Travel & Adventure",
  ]

  def human_size
    "#{size} páginas"
  end
end
