class Credit < ApplicationRecord
  belongs_to :user
  belongs_to :purchase

  validates :user, :amount, presence: true
end
