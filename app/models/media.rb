class Media < ApplicationRecord
  belongs_to :author, class_name: 'User', foreign_key: :user_id, touch: true

  validates :name, :description, :image, :price, :author, :category, :type, presence: true

  validates :price, numericality: { greater_than_or_equal_to: 0.01 }

  validates :size, presence: true, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
  }

  def disable(reason)
    self.removed = true
    self.published = false
    self.deleted_at = Time.now
    self.deleted_reason = reason
    save
  end
end
