class Music < Media
  CATEGORIES = [
    "Alternative",
    "Blues",
    "Children's Music",
    "Christian & Gospel",
    "Classical",
    "Comedy",
    "Country",
    "Dance",
    "Eletronic",
    "Fitness & Workout",
    "Hip-Hop/Rap",
    "New Artists",
    "Jazz",
    "Latino",
    "Live Music",
    "Metal",
    "Classical Hits",
    "Pop",
    "R&B/Soul",
    "Reggae",
    "Rock",
    "Singer/Songwritter",
    "Soundtrack",
    "World",
  ]

  def human_size
    "#{size} minutos"
  end
end
