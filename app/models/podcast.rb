class Podcast < Media
  CATEGORIES = [
    "Arts",
    "Business",
    "Comedy",
    "Education",
    "Games & Hobbies",
    "Government & Organizations",
    "Health",
    "Kids & Family",
    "Music",
    "News & Politics",
    "Religion & Spirituality",
    "Science & Medicine",
    "Society & Culture",
    "Sports & Recreation",
    "Technology",
    "TV & Film",
  ]

  def human_size
    "#{size} minutos"
  end
end
