class Purchase < ApplicationRecord
  before_create :discount_credits

  belongs_to :user
  belongs_to :media

  validates :user, :media, presence: true

  validates :price, presence: true, numericality: {
    greater_than_or_equal_to: 0.01,
  }

  validate :user_have_credits

  private

  def user_have_credits
    user.credits_amount >= media.price
  end

  def discount_credits
    user.credits.create(amount: media.price * -1.0)
  end
end
