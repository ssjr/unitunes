class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :medias, dependent: :nullify
  has_many :purchases, dependent: :nullify
  has_many :favorites, dependent: :destroy
  has_many :credits, dependent: :nullify

  validates :first_name, :last_name, presence: true

  def full_name
    "#{first_name} #{last_name}"
  end

  def desactive
    update(active: false)
  end

  def credits_amount
    credits.sum(:amount)
  end

  def active_for_authentication?
    super && active?
  end
end
