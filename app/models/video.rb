class Video < Media
  CATEGORIES = [
    "Kids & Family",
    "Comedy",
    "Action & Adventure",
    "Classics",
    "Documentary",
    "Drame",
    "Foreign",
    "Horror",
    "Independent",
    "Music",
    "Romance",
    "Sci-Fi & Fantasy",
    "Short Films",
    "Sports",
    "Thriller",
    "Western",
  ]

  def human_size
    "#{size} minutos"
  end
end
