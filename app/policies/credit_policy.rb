class CreditPolicy < ApplicationPolicy
  def index?
    user
  end

  def create?
    user
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
