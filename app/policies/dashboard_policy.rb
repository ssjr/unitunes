class DashboardPolicy < ApplicationPolicy
  def dashboard?
    user.is_a?(Admin)
  end
end
