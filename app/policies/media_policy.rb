class MediaPolicy < ApplicationPolicy
  def index?
    user
  end

  def update?
    record.author == user || user.is_a?(Admin)
  end

  def destroy?
    update?
  end

  def favorite?
    user
  end

  def purchase?
    user && !user.purchases.exists?(media: record)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
