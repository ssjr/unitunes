class UserPolicy < ApplicationPolicy
  def index?
    user && user.is_a?(Admin)
  end

  def create?
    user && user.is_a?(Admin)
  end

  def show?
    user && user.is_a?(Admin)
  end

  def update?
    user && user.is_a?(Admin)
  end

  def destroy?
    user && user.is_a?(Admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
