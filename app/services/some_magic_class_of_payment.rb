# here we can create a complex logic to process payment with a lot of
# options like credit card, bank slip, bank transfer, bit coin and more
class SomeMagicClassOfPayment
  attr_accessor :credit, :user, :params

  def initialize(user:, params:)
    self.user = user
    self.params = params
  end

  # in this method we make payment requests. but since this is just an example
  # we can just create credits and assume success
  def create
    @credit = current_user.credits.create(params)
  end
end
