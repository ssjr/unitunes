Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_for :users

  resources :users, except: %i(new create)

  resources :musics, controller: :medias do
    post :favorite
    post :purchase
  end

  resources :podcasts, controller: :medias do
    post :favorite
    post :purchase
  end

  resources :videos, controller: :medias do
    post :favorite
    post :purchase
  end

  resources :books, controller: :medias do
    post :favorite
    post :purchase
  end

  get :favorites, to: 'favorites#index'

  resources :credits, only: %i(index new create)

  get :dashboard, to: 'pages#dashboard'
  root to: redirect('/musics')
end
