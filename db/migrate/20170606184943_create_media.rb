class CreateMedia < ActiveRecord::Migration[5.1]
  def change
    create_table :media do |t|
      t.string :name
      t.text :description
      t.string :image
      t.decimal :price
      t.references :user, foreign_key: true
      t.string :category
      t.integer :size
      t.boolean :published, default: true
      t.datetime :deleted_at
      t.text :deleted_reason
      t.string :type

      t.timestamps
    end
  end
end
