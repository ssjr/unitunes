class CreatePurchases < ActiveRecord::Migration[5.1]
  def change
    create_table :purchases do |t|
      t.references :user, foreign_key: true
      t.references :media, foreign_key: true
      t.decimal :price

      t.timestamps
    end
  end
end
