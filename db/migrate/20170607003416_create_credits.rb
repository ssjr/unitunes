class CreateCredits < ActiveRecord::Migration[5.1]
  def change
    create_table :credits do |t|
      t.references :user, foreign_key: true
      t.decimal :amount
      t.references :purchases, foreign_key: true

      t.timestamps
    end
  end
end
