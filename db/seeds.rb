# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin = Admin.create!(email: 'admin@unitunes.br', password: '123123123', first_name: 'admin', last_name: 'admin')
author = Academic.create!(email: 'author@unitunes.br', password: '123123123', first_name: 'author', last_name: 'author')
academic = Academic.create!(email: 'academic@unitunes.br', password: '123123123', first_name: 'academic', last_name: 'academic')

200.times do
  klass = [Video, Book, Music, Podcast].sample
  klass.create!(author: author,
                name: "#{klass} name",
                description: "long #{klass} description",
                image: 'http://lorempixel.com/400/200/',
                category: klass::CATEGORIES.sample,
                price: 10,
                size: 10)
end
